﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_SOLID.Enums
{
    public enum ComparisonResult
    {
        Less,
        More,
        Equals,
        Exceeded
    }
}
