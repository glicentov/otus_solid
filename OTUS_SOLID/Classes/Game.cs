﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTUS_SOLID.Interfaces;
using System.Configuration;
using OTUS_SOLID.Enums;

namespace OTUS_SOLID.Classes
{
    class Game : IGame
    {
        private INumRandomGenerator _numRandomGenerator; // Генератор чисел
        private IUserGameInteraction _userGameInteraction; // Этот класс отвечает за взаимодействие с пользователем
        private int _totalAttemptsCount = Int32.Parse(ConfigurationManager.AppSettings["AttemptsCount"]);
        private int _curAttemptNum = 1; // счётчик количества попыток ввода от пользователя - должна быть миниму 1 попытка
        private int _userInput = default;
        private int _secret = default;
        private ComparisonResult _comparisonResult;
        private GameStatus _gameStatus = GameStatus.InProgress; 

        public Game(INumRandomGenerator randomGenerator, IUserGameInteraction userGameInteraction)
        {
            _numRandomGenerator = randomGenerator;
            _userGameInteraction = userGameInteraction;
        }

        public void RunGame()
        {
            GameProcesing();
        }
        public void GameProcesing()
        {
            _secret = _numRandomGenerator.GetNextRandomNum();
            while (_curAttemptNum <= _totalAttemptsCount && _gameStatus == GameStatus.InProgress)
            {
                _userInput = _userGameInteraction.inputRequest();
                compareInputToSecret(_userInput);
                _curAttemptNum++;
            }
            if (_gameStatus == GameStatus.InProgress && _curAttemptNum > _totalAttemptsCount)
            {
                _gameStatus = GameStatus.Exceeded;
                _comparisonResult = ComparisonResult.Exceeded;
                _userGameInteraction.giveResultToUser(_comparisonResult);
                Console.WriteLine($"The secret number was {_secret}");
            }    
        }
        public void compareInputToSecret(int userInput)
        {
            if (userInput == _secret)
            {
                _gameStatus = GameStatus.Won;
                _comparisonResult = ComparisonResult.Equals;
                _userGameInteraction.giveResultToUser(_comparisonResult);   
            }
            if (userInput > _secret)
            {
                _gameStatus = GameStatus.InProgress;
                _comparisonResult = ComparisonResult.More;
                _userGameInteraction.giveResultToUser(_comparisonResult, _totalAttemptsCount - _curAttemptNum);
            }
            if (userInput < _secret)
            {
                _gameStatus = GameStatus.InProgress;
                _comparisonResult = ComparisonResult.Less;
                _userGameInteraction.giveResultToUser(_comparisonResult, _totalAttemptsCount - _curAttemptNum);
            }
        }        
    }
}
