﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTUS_SOLID.Interfaces;
using OTUS_SOLID.Enums;
using System.Configuration;

namespace OTUS_SOLID.Classes
{
    class UserGameInteraction : IUserGameInteraction
    {

        // Запрашивает ввод от пользователя и возвращает введённое число
        public int inputRequest()
        {
            Console.WriteLine($"Please input the digit");
            Int32.TryParse(Console.ReadLine(), out int userInput);
            return userInput;
        }

        public void giveResultToUser(ComparisonResult result, int attemptsLeft = 0)
        {
            switch (result)
            {
                case ComparisonResult.Less:
                    Console.WriteLine($"Your result is less than was guessed! You have {attemptsLeft} attempts left!");
                    break;
                case ComparisonResult.More:
                    Console.WriteLine($"Your result is more than was guessed! You have {attemptsLeft} attempts left!");
                    break;
                case ComparisonResult.Equals:
                    Console.WriteLine("You won the Game! Congratulation!");
                    break;
                case ComparisonResult.Exceeded:
                    Console.WriteLine($"Your attempts were spent! You have lost the Game! Sorry");
                    break;
                default:
                    break;
            }
        }
    }
}
