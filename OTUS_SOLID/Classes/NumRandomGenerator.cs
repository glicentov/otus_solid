﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTUS_SOLID.Interfaces;
using System.Configuration;


namespace OTUS_SOLID.Classes
{
    class NumRandomGenerator : INumRandomGenerator
    {
        public int GetNextRandomNum()
        {
            Random random = new Random();
            // получаем минимум и максимум для генерации из файла конфигурации
            return random.Next(int.Parse(ConfigurationManager.AppSettings["RandomValueGeneratorMinValue"]),
                               int.Parse(ConfigurationManager.AppSettings["RandomValueGeneratorMaxValue"]));
        }
    }
}
