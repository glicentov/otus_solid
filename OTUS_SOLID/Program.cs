﻿using System;
using OTUS_SOLID.Classes;

namespace OTUS_SOLID
{
    class Program
    {
        static void Main(string[] args)
        {
            NumRandomGenerator numRandomGenerator = new NumRandomGenerator();
            UserGameInteraction userGameInteraction = new UserGameInteraction();
            Game game = new Game(numRandomGenerator, userGameInteraction);
            game.RunGame();
        }
    }
}
