﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTUS_SOLID.Enums;

namespace OTUS_SOLID.Interfaces
{
    /// <summary>
    /// Интерфейс для взаимодействия с пользователем 
    /// </summary>
    interface IUserGameInteraction 
    {
        public int inputRequest(); // Запрос на ввод числа и передача его в логику игры
        public void giveResultToUser(ComparisonResult result, int attemptsLeft = 0); // Передача инфо для юзера (Число - Меньше, Больше или Выигрыш)
    }
}
