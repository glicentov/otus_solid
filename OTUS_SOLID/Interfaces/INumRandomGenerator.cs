﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_SOLID.Interfaces
{
    interface INumRandomGenerator
    {
        public int GetNextRandomNum(); // Возвращает случайное число которое будет использоваться в игре
    }
}
