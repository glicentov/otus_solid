﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_SOLID.Interfaces
{
    interface IGame
    {
        public void RunGame(); // Запуск и процесс игры
        public void GameProcesing(); // Процессинг игры (Обработка результатов, запрос инф-ы, вывод и пр)
        public void compareInputToSecret(int userInput); // Сравнение ввода пользователя с загаданным числом
    }
}
